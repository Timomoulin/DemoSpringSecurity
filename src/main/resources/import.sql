insert into role (nom) values ('admin');
insert into role (nom) values ('client');

insert into utilisateur (role_id, email, mdp) values (1,'admin@email.com','$2a$10$MJn5SDUCgsm3XEyvELGQI.lcCzCXtxhA8hunr9jX9yvDd6/FkjxYO');
insert into utilisateur (role_id, email, mdp) values (2,'client@email.com','$2a$10$GM8MfrVFZ3BnjuXJbGYcSu.FsRUnyuHJyVB7es1qU.5mlk1EHp29m');
