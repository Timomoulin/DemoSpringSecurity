package com.example.antisechesecurity.model.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import com.example.antisechesecurity.model.entity.JetonResetMdp;

public interface JetonResetMdpRepository extends JpaRepository<JetonResetMdp,Long> {
    JetonResetMdp findByToken(String token);
}
