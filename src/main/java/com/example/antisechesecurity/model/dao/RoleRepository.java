package com.example.antisechesecurity.model.dao;

import com.example.antisechesecurity.model.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long> {
}