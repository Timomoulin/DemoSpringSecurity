package com.example.antisechesecurity.service;

import com.example.antisechesecurity.dto.PasswordDTO;
import com.example.antisechesecurity.dto.UtilisateurDTO;
import com.example.antisechesecurity.model.dao.RoleRepository;
import com.example.antisechesecurity.model.dao.UtilisateurRepository;
import com.example.antisechesecurity.model.entity.Utilisateur;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.NoSuchElementException;


@Service
public class UtilisateurService {
    private UtilisateurRepository utilisateurRepository;
    private RoleRepository roleRepository;
    private PasswordEncoder encoder;
@Autowired
    public UtilisateurService(UtilisateurRepository utilisateurRepository,RoleRepository roleRepository, PasswordEncoder encoder) {
        this.utilisateurRepository = utilisateurRepository;
        this.roleRepository= roleRepository;
        this.encoder=encoder;
    }

    public Utilisateur inscrireClient(Utilisateur user){

    user.setRole(roleRepository.findById(2l).get());
    user.setMdp(encoder.encode(user.getMdp()));
        System.out.println(user.getMdp());
    return utilisateurRepository.save(user);
    }

    public Utilisateur saveNewPassword(Utilisateur user){
        if (user.getId() != null) {


            user.setMdp(encoder.encode(user.getMdp()));
            return utilisateurRepository.save(user);
        }

            throw new NoSuchElementException();
    }

    public Utilisateur convertToEntity(UtilisateurDTO dto){
        Utilisateur utilisateur;
    if(dto.getId()==null){
        utilisateur= new Utilisateur();
    }
    else{
        utilisateur= utilisateurRepository.findById(dto.getId()).orElse(new Utilisateur());
    }
    utilisateur.setEmail(dto.getEmail());
    utilisateur.setMdp(dto.getMdp1NonEncoder());
    utilisateur.setRole(dto.getRole());
    return utilisateur;
    }

    public Utilisateur convertToEntity(PasswordDTO dto){
        Utilisateur utilisateur;
        if(dto.getToken()==null){
            throw new NoSuchElementException();
        }
        else{
            utilisateur= utilisateurRepository.findByJetonResetMdps_TokenIgnoreCase(dto.getToken()).orElseThrow();
            utilisateur.setMdp(dto.getMdp1());
        }



        return utilisateur;
    }


    public Utilisateur findByEmail(String email){
    return this.utilisateurRepository.findByEmail(email);
    }
}
