package com.example.antisechesecurity.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig {


    @Bean
    public PasswordEncoder getEncoder(){
        return new BCryptPasswordEncoder();
    }

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity httpSecurity) throws Exception {
        return httpSecurity
                .csrf(csrf -> csrf.disable())
                .formLogin(form -> form
                        .loginPage("/login").defaultSuccessUrl("/accueil").failureUrl("/login")
                        .permitAll()
                )
                .logout(logout-> logout
                        .logoutUrl("/logout")
                        .permitAll()
                )
                .authorizeHttpRequests(auth -> auth
                        .requestMatchers("/reset-password/**","/forgot-password","/accueil","/inscription","/css/**", "/js/**", "/img/**", "/favicon.ico").permitAll()
                        .requestMatchers(HttpMethod.POST,("/inscription")).permitAll()
                        //Interdit la page si l'utilisateur n'est pas admin
                        .requestMatchers("/admin/**").hasAuthority("admin")
                        .requestMatchers("/client/**").hasAnyAuthority("admin","client")
                        .anyRequest().authenticated()
                )
                .build();
    }

}
