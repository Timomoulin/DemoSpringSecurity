package com.example.antisechesecurity.controller;


import com.example.antisechesecurity.dto.PasswordDTO;
import com.example.antisechesecurity.model.entity.JetonResetMdp;
import com.example.antisechesecurity.model.entity.Utilisateur;
import com.example.antisechesecurity.service.JavaSenderMail;
import com.example.antisechesecurity.service.JetonResetMdpService;
import com.example.antisechesecurity.service.UtilisateurService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.mail.javamail.JavaMailSender;

import java.util.UUID;

@Controller
public class ResetPasswordController {


    private JetonResetMdpService tokenService;

    private JavaSenderMail javaSenderMail;


    private UtilisateurService utilisateurService;
@Autowired
    public ResetPasswordController(JetonResetMdpService tokenService, JavaSenderMail javaSenderMail, UtilisateurService utilisateurService) {
        this.tokenService = tokenService;
        this.javaSenderMail = javaSenderMail;
        this.utilisateurService = utilisateurService;
    }

    /**
     * Formulaire en cas d'oublie
     * @return
     */
    @GetMapping("/forgot-password")
    public String showForgotPasswordForm() {
        return "/visiteur/mdpoublier";
    }

    /**
     * Traitement du formulaire
     * @param email
     * @return
     */
    @PostMapping("/forgot-password")
    public String processForgotPassword(@RequestParam("email") String email) {
        Utilisateur utilisateur = utilisateurService.findByEmail(email);
        if (utilisateur != null) {
            // Générez un jeton de réinitialisation de mot de passe

            String token = tokenService.createToken(utilisateur).getToken();

            // Envoyez le jeton au courriel de l'utilisateur et incluez un lien pour réinitialiser le mot de passe
            javaSenderMail.sendPasswordResetEmail(utilisateur.getEmail(),token );
            // Envoyez également le lien vers une page de réinitialisation de mot de passe
            // ...
        }
        return "redirect:/forgot-password?success";
    }

    @GetMapping("/reset-password")
    public String showResetPasswordForm(@RequestParam("token") String token, Model model) {
        JetonResetMdp resetToken = tokenService.findByToken(token);
        if (resetToken != null) {
            PasswordDTO dto= new PasswordDTO(token);
            model.addAttribute("passwordDto", dto);
            return "/visiteur/resetmdp";
        }
        return "redirect:/forgot-password?invalidToken";
    }

    @PostMapping("/reset-password")
    public String processResetPassword(@ModelAttribute PasswordDTO passwordDTO)  {
        JetonResetMdp resetToken = tokenService.findByToken(passwordDTO.getToken());
        if (resetToken != null) {
            //TODO verification de l'expiration du token
            Utilisateur utilisateur=utilisateurService.convertToEntity(passwordDTO);
            utilisateurService.inscrireClient(utilisateur);
            tokenService.deleteToken(resetToken);
            return "redirect:/login?passwordResetSuccess";
        }
        return "redirect:/login?passwordResetError";
    }



}