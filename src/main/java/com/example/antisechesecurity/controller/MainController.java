package com.example.antisechesecurity.controller;

import com.example.antisechesecurity.dto.UtilisateurDTO;
import com.example.antisechesecurity.service.UtilisateurService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class MainController {

    private UtilisateurService utilisateurService;
@Autowired
    public MainController(UtilisateurService utilisateurService) {
        this.utilisateurService = utilisateurService;
    }

    /**
     * Affiche le formulaire de Login TODO
     *
     */
    @GetMapping("/login")
public String login(){
    return "visiteur/login";
}

    /**
     * Affiche le formulaire d'inscription
     *
     */
    @GetMapping("/inscription")
public String inscription(Model model){
        model.addAttribute("utilisateur",new UtilisateurDTO());
        return "visiteur/inscription";
}

@PostMapping("/inscription")
public String traitementInscription(@Valid @ModelAttribute UtilisateurDTO utilisateur,BindingResult bindingResult, RedirectAttributes redirectAttributes){
        if(bindingResult.hasErrors()){
            redirectAttributes.addFlashAttribute("errorMessage", "Le formulaire comporte une erreur");
            return "visiteur/inscription";
        }

        utilisateurService.inscrireClient(utilisateurService.convertToEntity(utilisateur));
        redirectAttributes.addFlashAttribute("successMessage", "Inscription réusie");
        return "redirect:/login";

}
    /**
     * EXEMPLE DE PAGE VISITEUR
     * Dirige vers la page d'acceuil (visible par tout les utilisateurs)
     */
    @GetMapping("/accueil")
    public String accueil(){
        return "visiteur/accueil";
    }

    /**
     * EXEMPLE DE PAGE CLIENT/ADMIN
     * Dirige vers la page le dashboard du client ou admin (visible seulement si l'utilisateur est un client ou admin)
     */
    @GetMapping("/client/dashboard")
    public String clientDash(){
        return "client/dashboard";
    }

    /**
     * EXEMPLE DE PAGE ADMIN
     * Dirige vers une page d'administration des utilisateurs (visible seulement si l'utilisateur est un admin)
     */
    @GetMapping("/admin/utilisateur")
    public String admin(){
        return "admin/utilisateur/index";
    }
}
