package com.example.antisechesecurity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AntisecheSecurityApplication {

    public static void main(String[] args) {
        SpringApplication.run(AntisecheSecurityApplication.class, args);
    }

}
